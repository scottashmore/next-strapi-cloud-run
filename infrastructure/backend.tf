terraform {
  backend "gcs" {
    bucket = "sashmore11-terraform-admin"
    prefix = "terraform/state"
  }
}