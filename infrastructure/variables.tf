variable "gcp_project_name" {
  type = string
  description = "Name of the Google Cloud Platform Project to be created."
  default = "next-strapi-cloud-run"
}

variable "gcp_region" {
  type = string
  description = "Google Cloud Region that the project should be created within"
  default = "europe-west1"
}

variable "google_project_services" {
  type = list(string)
  description = "Google Project Services to be enabled"
  default = [
    "run.googleapis.com",
    "containerregistry.googleapis.com",
    "sqladmin.googleapis.com",
    "logging.googleapis.com"
  ]
}

variable "pipelines_ip_addresses" {
  type = list(string)
  description = "Bitbucket Pipelines IP addresses for remote connections to SQL instances upon deployment"
  default = [
    "34.199.54.113/32",
    "34.232.25.90/32",
    "34.232.119.183/32",
    "34.236.25.177/32",
    "35.171.175.212/32",
    "52.54.90.98/32",
    "52.202.195.162/32",
    "52.203.14.55/32",
    "52.204.96.37/32",
    "34.218.156.209/32",
    "34.218.168.212/32",
    "52.41.219.63/32",
    "35.155.178.254/32",
    "35.160.177.10/32",
    "34.216.18.129/32"
  ]
}

variable "bitbucket_username" {
  type = string
  description = "Account ID of the Bitbucket repository owner (the organization.)"
  default = "sashmore11"
}

variable "bitbucket_password" {
  type = string
  description = "Password of the Bitbucket app"
  sensitive = true
}

variable "bitbucket_repository_slug" {
  type = string
  description = "Slug of the repository within the organization"
  default = "next-strapi-cloud-run"
}