output "gcp_project_id" {
  value = google_project.project.project_id
}

output "local_database_service_account_key_json" {
  value = base64decode(google_service_account_key.local_database.private_key)
}

output "cloud_sql_root_user_password" {
  value = random_password.cms_sql_root_password.result
}

output "cloud_sql_local_user_password" {
  value = random_password.cms_sql_local_password.result
}