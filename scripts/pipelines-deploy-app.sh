#! /bin/bash

echo ""
echo "Building and deploying the app..."

cd app || exit

image="eu.gcr.io/$GCLOUD_PROJECT_ID/$BITBUCKET_DEPLOYMENT_ENVIRONMENT"
existing_tags=$(gcloud container images list-tags --filter="tags:app" --format=json "$image")

if [[ "$existing_tags" == "[]" ]]; then
  docker build . \
    --tag "$image:app" \
    --build-arg=CMS_GRAPHQL_URL="$CMS_GRAPHQL_URL"
else
  docker pull "$image:app"
  docker build . \
    --tag "$image:app" \
    --cache-from "$image:app" \
    --build-arg=CMS_GRAPHQL_URL="$CMS_GRAPHQL_URL"
fi

docker push "$image:app"

gcloud run deploy "$GCLOUD_PROJECT_ID-app" \
  --image "$image:app" \
  --platform managed \
  --region "$GCP_REGION" \
  --update-env-vars CMS_GRAPHQL_URL="$CMS_GRAPHQL_URL" \
  --port 3000

cd ..