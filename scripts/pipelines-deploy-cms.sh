#! /bin/bash

echo ""
echo "Building and deploying the CMS..."

cd cms || exit

image="eu.gcr.io/$GCLOUD_PROJECT_ID/$BITBUCKET_DEPLOYMENT_ENVIRONMENT"
existing_tags=$(gcloud container images list-tags --filter="tags:cms" --format=json "$image")

if [[ "$existing_tags" == "[]" ]]; then
  docker build . --tag "$image:cms"
else
  docker pull "$image:cms"
  docker build . --tag "$image:cms" --cache-from "$image:cms"
fi

docker push "$image:cms"

gcloud run deploy "$GCLOUD_PROJECT_ID-cms" \
  --image "$image:cms" \
  --platform managed \
  --region "$GCP_REGION" \
  --update-env-vars DB_NAME="$DB_NAME" \
  --update-env-vars DB_SOCKET_PATH="$DB_SOCKET_PATH" \
  --update-env-vars DB_USERNAME="$DB_USERNAME" \
  --update-env-vars DB_PASSWORD="$DB_PASSWORD"

cd ..